package com.yoona.quick_start.common.configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/29
 * @Time: 10:17 上午
 * @Msg: MyBatisPlus分页插件
 */
@Configuration
public class MyBatisPlusConfiguration {

    /**
     * 分页插件
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }


}
