package com.yoona.quick_start.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/21
 * @Time: 5:12 下午
 * @Msg: 角色枚举
 */
@Getter
@AllArgsConstructor
public enum RoleEnums {

    /**
     * 枚举系统的角色
     */
    ADMIN(1,"管理员"),
    NORMAL_USER(2,"普通用户");

    private final int code;
    private final String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
