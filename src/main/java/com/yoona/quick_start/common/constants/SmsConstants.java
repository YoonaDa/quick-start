package com.yoona.quick_start.common.constants;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/21
 * @Time: 6:14 下午
 * @Msg: 腾讯短信服务常量类
 */
public class SmsConstants {

    /**
     * APP_ID
     */

    public static final Integer APP_ID= 1400259345;

    /**
     * APP_KEY
     */

    public static final String APP_KEY="2cf87aba994b97e039dd1618a32746cb";

    /**
     * 短信签名
     */

    public static final String SMS_SIGN="236368";

    /**
     * 模板id
     */

    public static final Integer TEMPLATE_ID=422233;
}
