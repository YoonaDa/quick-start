package com.yoona.quick_start.common.constants;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/21
 * @Time: 4:42 下午
 * @Msg: JWT常量类
 */

public class JwtConstants {

    /**
     * token过期时间: 7天(以毫秒ms计)
     */

    public static final long EXPIRATION_TIME = 7*24*60*60*1000;

    /**
     * JWT密码
     */

    public static final String SECRET = "SystemSecret";

    /**
     * Token前缀
     */

    public static final String TOKEN_PREFIX = "Bearer";

    /**
     * 存放Token的Header
     */

    public static final String HEADER_STRING = "Authorization";

    /**
     * 配置不需要认证的接口
     */

    public static final String ANT_MATCHERS= "/swagger-ui.html,/swagger-resources/**,/images/**,/webjars/**,/v2/api-docs,/configuration/ui,/configuration/security," +
            "/sms/**,/sys-user/loginOrRegisterByMobilePhoneNumber";

}

