package com.yoona.quick_start.common.handler;

import com.alibaba.fastjson.JSON;
import com.yoona.quick_start.common.enums.CommonEnums;
import com.yoona.quick_start.common.response.SystemResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/21
 * @Time: 5:41 下午
 * @Msg: 用户无权限访问处理类
 * (用来解决认证过的用户访问无权限资源时的异常)无权访问返回的 JSON 格式数据给前端（否则为 403 html 页面）
 */
@Component
public class UserAuthAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(JSON.toJSONString(SystemResponse.response(CommonEnums.PERMISSION_DENNY)));
    }
}
