package com.yoona.quick_start.common.handler;

import com.alibaba.fastjson.JSON;
import com.yoona.quick_start.common.enums.CommonEnums;
import com.yoona.quick_start.common.response.SystemResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/22
 * @Time: 2:13 下午
 * @Msg: 用户未登录处理类
 */
@Component
public class UserAuthenticationEntryPointHandler implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(JSON.toJSONString(SystemResponse.response(CommonEnums.GLOBAL_ERR_NO_SIGN_IN)));
    }
}
