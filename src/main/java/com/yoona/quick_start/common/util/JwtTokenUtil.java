package com.yoona.quick_start.common.util;

import com.alibaba.fastjson.JSON;
import com.yoona.quick_start.common.constants.JwtConstants;
import com.yoona.quick_start.modules.security.entity.SelfUser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/27
 * @Time: 3:29 下午
 * @Msg: Jwt工具类
 */
@Slf4j
public class JwtTokenUtil {


    public JwtTokenUtil() {
    }

    /**
     * 生成token
     * @param selfUser
     * @return
     */
    public static String createAccessToken(SelfUser selfUser) {
        // 登陆成功生成JWT
        return Jwts.builder()
                // 放入用户名和用户ID
                .setId(selfUser.getUid() + "")
                // 主题
                .setSubject(selfUser.getUsername())
                // 签发时间
                .setIssuedAt(new Date())
                // 签发者
                .setIssuer("sans")
                // 自定义属性 放入用户拥有权限
                .claim("authorities", JSON.toJSONString(selfUser.getAuthorities()))
                // 失效时间
                .setExpiration(new Date(System.currentTimeMillis() + JwtConstants.EXPIRATION_TIME))
                // 签名算法和密钥
                .signWith(SignatureAlgorithm.HS512, JwtConstants.SECRET)
                .compact();
    }
}