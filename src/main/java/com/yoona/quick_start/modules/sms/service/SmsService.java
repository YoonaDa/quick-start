package com.yoona.quick_start.modules.sms.service;

import com.yoona.quick_start.common.response.SystemResponse;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/22
 * @Time: 9:50 上午
 * @Msg:
 */
public interface SmsService {

    /**
     * 传入mobilePhoneNumber,用来发送验证码
     * @param mobilePhoneNumber
     * @return
     *
     */
    SystemResponse sendMessage(String mobilePhoneNumber);


    /**
     * 校验用户的验证码
     * @param mobilePhoneNumber
     * @param verificationCode
     * @return
     */

    SystemResponse checkVerificationCode(String mobilePhoneNumber, String verificationCode);


}
