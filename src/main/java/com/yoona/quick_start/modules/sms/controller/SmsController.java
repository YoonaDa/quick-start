package com.yoona.quick_start.modules.sms.controller;

import com.yoona.quick_start.common.response.SystemResponse;
import com.yoona.quick_start.modules.sms.service.SmsService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/22
 * @Time: 11:02 上午
 * @Msg:
 */
@RestController
@RequestMapping("/sms")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SmsController {

    private final SmsService smsService;

    @ApiOperation("发送手机验证码")
    @PostMapping(value = "/sendCode")
    public SystemResponse sendMessage(@RequestParam("mobilePhoneNumber") String mobilePhoneNumber) {
        return smsService.sendMessage(mobilePhoneNumber);
    }

    @ApiOperation("校验验证码")
    @PostMapping(value = "/checkIsCorrectCode")
    public SystemResponse checkVerificationCode(@RequestParam("mobilePhoneNumber") String mobilePhoneNumber, @RequestParam("verificationCode") String verificationCode) {
        return smsService.checkVerificationCode(mobilePhoneNumber, verificationCode);
    }

}
