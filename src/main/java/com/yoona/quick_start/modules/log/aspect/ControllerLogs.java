package com.yoona.quick_start.modules.log.aspect;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/31
 * @Time: 10:57 上午
 * @Msg: 打印Controller层的日志
 */
@Aspect
@Component
@Slf4j
public class ControllerLogs {

    @Pointcut("execution(public * com.yoona.quick_start.modules.*.controller.*.*(..))")
    public void controller(){

    }

    @Before("controller()")
    public void before(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        String method = signature.getDeclaringTypeName() + "." + signature.getName();
        log.info("---------------------------------------------------------------------------------------------------------------------------------------");
        log.info("当前执行controller的方法:" + method);
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            log.info("参数: " + arg);
        }
    }

    @AfterReturning(pointcut = "controller()", returning = "ret")
    public void after(Object ret){
        log.info("controller返回结果:" + JSON.toJSONString(ret));
        log.info("---------------------------------------------------------------------------------------------------------------------------------------");
    }
}
