package com.yoona.quick_start.modules.log.persistence;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/31
 * @Time: 11:15 上午
 * @Msg: 自定义日志注解
 */
import java.lang.annotation.*;

@Target(ElementType.METHOD) //注解放置的目标位置,METHOD是可注解在方法级别上
@Retention(RetentionPolicy.RUNTIME) //注解在哪个阶段执行
@Documented //生成文档
public @interface MyLog {
    String value() default "";
}