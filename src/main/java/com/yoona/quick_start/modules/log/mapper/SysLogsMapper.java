package com.yoona.quick_start.modules.log.mapper;

import com.yoona.quick_start.modules.log.entity.SysLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统日志表 Mapper 接口
 * </p>
 *
 * @author yoonada
 * @since 2020-07-31
 */
public interface SysLogsMapper extends BaseMapper<SysLogs> {

}
