package com.yoona.quick_start.modules.log.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 系统日志表
 * </p>
 *
 * @author yoonada
 * @since 2020-07-31
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SysLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * ip地址
     */
    private String ip;

    /**
     * 方法
     */
    private String method;

    /**
     * 操作
     */
    private String operation;

    /**
     * 请求方式
     */
    private String requestMode;

    /**
     * 访问的URL
     */
    private String url;

    /**
     * 用户名
     */
    private String userName;


}
