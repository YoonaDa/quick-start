package com.yoona.quick_start.modules.security.vo;

import lombok.*;

import javax.validation.constraints.NotBlank;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/27
 * @Time: 5:45 下午
 * @Msg:
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginOrRegisterVO {

    @NotBlank(message = "手机号码不能为空")
    private String mobilePhoneNumber;

    @NotBlank(message = "验证码不能为空")
    private String verificationCode;

}
