package com.yoona.quick_start.modules.security.service;

import com.yoona.quick_start.common.response.SystemResponse;
import com.yoona.quick_start.modules.security.entity.SysRole;
import com.yoona.quick_start.modules.security.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yoona.quick_start.modules.security.vo.UserLoginOrRegisterVO;

import java.util.List;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author yoonada
 * @since 2020-07-27
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 根据username查询
     * @param username
     * @return
     */

    SysUser selectUserByName(String username);

    /**
     * 根据用户id查询对应的角色
     * @param uid
     * @return
     */

    List<SysRole> selectSysRoleByUserId(String uid);

    /**
     * 用手机号码来登录/注册
     * @param userLoginOrRegisterVO
     * @return
     */

    SystemResponse loginOrRegisterByMobilePhoneNumber(UserLoginOrRegisterVO userLoginOrRegisterVO);

    /**
     * 更新用户信息
     * @param sysUser
     * @return
     */

    SystemResponse updateUserInfo(SysUser sysUser);
}
