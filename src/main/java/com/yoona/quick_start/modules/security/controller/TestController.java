package com.yoona.quick_start.modules.security.controller;

import com.yoona.quick_start.common.enums.CommonEnums;
import com.yoona.quick_start.common.response.SystemResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/27
 * @Time: 5:10 下午
 * @Msg: 测试权限Controller
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @ApiOperation("测试管理员权限")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/testAdmin")
    public SystemResponse testAdmin(){
        return SystemResponse.response(CommonEnums.SUCCESS);
    }

    @ApiOperation("测试普通用户权限")
    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/testUser")
    public SystemResponse testUser(){
        return SystemResponse.response(CommonEnums.SUCCESS);
    }

    @ApiOperation("测试管理员或普通权限")
    @PreAuthorize("hasAnyAuthority('USER','ADMIN')")
    @GetMapping("/testUserAndAdmin")
    public SystemResponse testUserAndAdmin(){
        return SystemResponse.response(CommonEnums.SUCCESS);
    }

}
