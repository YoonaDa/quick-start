package com.yoona.quick_start.modules.security.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/28
 * @Time: 11:24 上午
 * @Msg: 更新用户信息VO
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUserInfoVO {

    /**
     * 用户id
     */
    @NotBlank(message = "uid不能为空")
    private String uid;

    /**
     * 电子邮箱
     */
    @Email(message = "邮箱不正确")
    private String email;

    /**
     * 头像地址
     */
    private String avatarUrl;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 备注
     */
    private String remark;


}
