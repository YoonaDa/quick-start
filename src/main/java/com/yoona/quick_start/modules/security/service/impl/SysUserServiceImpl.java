package com.yoona.quick_start.modules.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yoona.quick_start.common.enums.CommonEnums;
import com.yoona.quick_start.common.response.SystemResponse;
import com.yoona.quick_start.common.util.JwtTokenUtil;
import com.yoona.quick_start.modules.security.entity.SelfUser;
import com.yoona.quick_start.modules.security.entity.SysRole;
import com.yoona.quick_start.modules.security.entity.SysUser;
import com.yoona.quick_start.modules.security.entity.SysUserRole;
import com.yoona.quick_start.modules.security.mapper.SysUserMapper;
import com.yoona.quick_start.modules.security.mapper.SysUserRoleMapper;
import com.yoona.quick_start.modules.security.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yoona.quick_start.modules.security.vo.UserLoginOrRegisterVO;
import com.yoona.quick_start.modules.sms.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author yoonada
 * @since 2020-07-27
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SelfUserDetailsServiceImpl selfUserDetailsService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SmsService smsService;

    /**
     * 用户账号状态
     */
    public static final String STATUS = "PROHIBIT";


    /**
     * 根据username查询信息
     * @param username
     * @return
     */
    @Override
    public SysUser selectUserByName(String username) {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SysUser::getUsername, username);
        return this.baseMapper.selectOne(queryWrapper);
    }

    /**
     * 根据uid查询对应的角色
     * @param uid
     * @return
     */
    @Override
    public List<SysRole> selectSysRoleByUserId(String uid) {
        return this.baseMapper.selectSysRoleByUserId(uid);
    }

    /**
     * 用手机号码和验证码登录/注册
     *
     * @param userLoginOrRegisterVO
     * @return 1、校验验证码:校验失败则返回,校验成功继续
     * 校验成功:
     * 2、判断手机号码所对应的账号是否已经存在
     * 3、不存在则注册并登录
     * 4、存在则直接登录
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public SystemResponse loginOrRegisterByMobilePhoneNumber(UserLoginOrRegisterVO userLoginOrRegisterVO) {
        String mobilePhoneNumber = userLoginOrRegisterVO.getMobilePhoneNumber();
        String verificationCode = userLoginOrRegisterVO.getVerificationCode();
//        校验验证码
        SystemResponse response = smsService.checkVerificationCode(mobilePhoneNumber, verificationCode);
        if (response.getCode() == CommonEnums.CHECK_VERIFICATION_CODE_SUCCESS.getCode()) {
//            校验成功
//            判断手机号码所对应的账号是否已经存在
            SelfUser selfUser = getSelfUser(mobilePhoneNumber);
            if (selfUser == null) {
//              账号不存在,这里做注册操作
                SysUser sysUser = SysUser.builder().createTime(new Date()).updateTime(new Date()).username(mobilePhoneNumber).password(bCryptPasswordEncoder.encode(mobilePhoneNumber)).phone(mobilePhoneNumber).status("NORMAL").build();
                try {
                    int num = sysUserMapper.insert(sysUser);
                    if (num == 1) {
                        SelfUser user = getSelfUser(mobilePhoneNumber);
                        SysUserRole sysUserRole = SysUserRole.builder().roleId("2").userId(user.getUid()).build();
                        sysUserRoleMapper.insert(sysUserRole);
//                    注册成功,进行登录并生成token
                        SelfUser userInfo = getSelfUser(mobilePhoneNumber);
                        // 角色集合
                        Set<GrantedAuthority> authorities = getGrantedAuthorities(userInfo);
                        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userInfo, mobilePhoneNumber, authorities);
                        String token = JwtTokenUtil.createAccessToken((SelfUser) usernamePasswordAuthenticationToken.getPrincipal());
                        return SystemResponse.response(CommonEnums.LOGIN_SUCCESS, token);
                    }
                } catch (Exception e) {
//                    注册异常,回滚
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    e.printStackTrace();
                }
                return SystemResponse.response(CommonEnums.INTERNAL_SERVER_ERROR);
            } else {
//                账号存在,登录
                SelfUser userInfo = getSelfUser(mobilePhoneNumber);
                // 判断用户账号状态是否为已禁用
                if (STATUS.equals(userInfo.getStatus())) {
                    return SystemResponse.response(CommonEnums.ACCOUNT_HAS_BEEN_FROZEN);
                }
                Set<GrantedAuthority> authorities = getGrantedAuthorities(userInfo);
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userInfo, selfUser.getPassword(), authorities);
                String token = JwtTokenUtil.createAccessToken((SelfUser) usernamePasswordAuthenticationToken.getPrincipal());
                return SystemResponse.response(CommonEnums.LOGIN_SUCCESS, token);
            }
        } else {
            return SystemResponse.response(CommonEnums.CHECK_VERIFICATION_CODE_FAILED);
        }
    }

    private Set<GrantedAuthority> getGrantedAuthorities(SelfUser userInfo) {
        // 角色集合
        Set<GrantedAuthority> authorities = new HashSet<>();
        // 查询用户角色
        List<SysRole> sysRoleList = sysUserService.selectSysRoleByUserId(userInfo.getUid());
        for (SysRole sysRole : sysRoleList) {
            authorities.add(new SimpleGrantedAuthority(sysRole.getRoleName()));
        }
        userInfo.setAuthorities(authorities);
        return authorities;
    }

    private SelfUser getSelfUser(String mobilePhoneNumber) {
        return selfUserDetailsService.loadUserByUsername(mobilePhoneNumber);
    }

    /**
     * 更新用户信息
     * @param sysUser
     * @return
     */

    @Override
    public SystemResponse updateUserInfo(SysUser sysUser) {
        sysUser.setUpdateTime(new Date());
        int i = this.baseMapper.updateById(sysUser);
        if (i == 0){
            return SystemResponse.response(CommonEnums.UPDATE_ERROR);
        }
        return SystemResponse.response(CommonEnums.UPDATE_SUCCESS);
    }

}
