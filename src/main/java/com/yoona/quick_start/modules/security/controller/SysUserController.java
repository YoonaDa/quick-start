package com.yoona.quick_start.modules.security.controller;


import com.yoona.quick_start.common.enums.CommonEnums;
import com.yoona.quick_start.common.response.SystemResponse;
import com.yoona.quick_start.modules.log.persistence.MyLog;
import com.yoona.quick_start.modules.security.entity.SelfUser;
import com.yoona.quick_start.modules.security.entity.SysUser;
import com.yoona.quick_start.modules.security.service.SysUserService;
import com.yoona.quick_start.modules.security.vo.UpdateUserInfoVO;
import com.yoona.quick_start.modules.security.vo.UserLoginOrRegisterVO;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 系统用户表 前端控制器
 * </p>
 *
 * @author yoonada
 * @since 2020-07-27
 */
@RestController
@RequestMapping("/sys-user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysUserController {

    private final SysUserService sysUserService;

    /**
     * 使用手机号码和验证码进行注册/登录
     * @param userLoginOrRegisterVO
     * @return
     */

    @ApiOperation("注册/登录")
    @PostMapping("/loginOrRegisterByMobilePhoneNumber")
    public SystemResponse loginOrRegisterByMobilePhoneNumber(@Valid @RequestBody UserLoginOrRegisterVO userLoginOrRegisterVO){
        return sysUserService.loginOrRegisterByMobilePhoneNumber(userLoginOrRegisterVO);
    }

    /**
     * 获取用户信息
     * @return
     */

    @MyLog("获取用户信息")
    @ApiOperation("获取用户信息")
    @GetMapping("/userInfo")
    public SystemResponse getUserInfo(){
        SelfUser userDetails = (SelfUser) SecurityContextHolder.getContext().getAuthentication() .getPrincipal();
        SysUser sysUser = sysUserService.selectUserByName(userDetails.getUsername());
        userDetails.setPhone(sysUser.getPhone());
        userDetails.setNickName(sysUser.getNickName());
        userDetails.setAvatarUrl(sysUser.getAvatarUrl());
        userDetails.setEmail(sysUser.getEmail());
        userDetails.setRemark(sysUser.getRemark());
        userDetails.setStatus(sysUser.getStatus());
        userDetails.setCreateTime(sysUser.getCreateTime());
        userDetails.setUpdateTime(sysUser.getUpdateTime());
        return SystemResponse.response(CommonEnums.SUCCESS,userDetails);
    }

    /**
     * 编辑用户信息
     */

    @MyLog("更新用户信息")
    @ApiOperation("更新用户信息")
    @PutMapping("/updateUserInfo")
    public SystemResponse updateUserInfo(@Valid @RequestBody UpdateUserInfoVO updateUserInfoVO){
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(updateUserInfoVO,sysUser);
        return sysUserService.updateUserInfo(sysUser);
    }




}
