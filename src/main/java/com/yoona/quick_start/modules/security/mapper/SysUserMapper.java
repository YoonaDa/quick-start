package com.yoona.quick_start.modules.security.mapper;

import com.yoona.quick_start.modules.security.entity.SysRole;
import com.yoona.quick_start.modules.security.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author yoonada
 * @since 2020-07-27
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 根据uid查询对应的角色
     * @param uid
     * @return
     */
    List<SysRole> selectSysRoleByUserId(String uid);

}
