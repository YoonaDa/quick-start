package com.yoona.quick_start.modules.security.mapper;

import com.yoona.quick_start.modules.security.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yoonada
 * @since 2020-07-27
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
