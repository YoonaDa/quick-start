package com.yoona.quick_start.modules.security.service.impl;

import com.yoona.quick_start.modules.security.entity.SelfUser;
import com.yoona.quick_start.modules.security.entity.SysUser;
import com.yoona.quick_start.modules.security.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @Email: m15602498163@163.com
 * @Author: yoonada
 * @Date: 2020/7/27
 * @Time: 2:56 下午
 * @Msg:
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SelfUserDetailsServiceImpl implements UserDetailsService {

    private final SysUserService sysUserService;

    /**
     * 查询用户信息
     */
    @Override
    public SelfUser loadUserByUsername(String username) throws UsernameNotFoundException {
        // 查询用户信息
        SysUser sysUser =sysUserService.selectUserByName(username);
        if (sysUser!=null){
            // 组装参数
            SelfUser selfUser = new SelfUser();
            BeanUtils.copyProperties(sysUser,selfUser);
            return selfUser;
        }
        return null;
    }
}