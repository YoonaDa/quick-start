/*
 Navicat Premium Data Transfer

 Source Server         : 120.78.207.19
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : 120.78.207.19:3307
 Source Schema         : quick-start-v2

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 31/07/2020 11:30:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `sys_logs`;
CREATE TABLE `sys_logs` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `ip` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'ip地址',
  `method` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '方法',
  `operation` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '操作',
  `request_mode` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '请求方式',
  `url` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '访问的URL',
  `user_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='系统日志表';

-- ----------------------------
-- Records of sys_logs
-- ----------------------------
BEGIN;
INSERT INTO `sys_logs` VALUES (1, '2020-07-31 11:27:42', '127.0.0.1', 'com.yoona.quick_start.modules.security.controller.SysUserController.getUserInfo', '获取用户信息', 'GET', 'http://localhost:9999/api/sys-user/userInfo', '15602498163');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(255) NOT NULL COMMENT '角色名称',
  `description` varchar(255) NOT NULL COMMENT '角色描述',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, 'ADMIN', '管理员');
INSERT INTO `sys_role` VALUES (2, 'USER', '普通用户');
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `uid` varchar(60) NOT NULL COMMENT '用户id',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(255) DEFAULT NULL COMMENT '电子邮箱',
  `avatar_url` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '昵称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` varchar(20) DEFAULT NULL COMMENT '账户状态',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES ('ce1c735a9889572280feb315ec737bfd', '15602498163', '$2a$10$jZf.TPI5TOcDoJ6kTZ40SuQd7YCsk2jYNyg6H2Ry1CMcHtv9zs2vm', '15602498163', '970757399@qq.com', 'https://wx.qlogo.cn/mmopen/vi_32/nTJeT8ejelUQdnp3G9ibzP5eCte18ibo159glaA6O37Iia6anuV4UOhNqgBUobc0FiapiabaFVpIxnTpGrVgyLrswFw/132', 'YoonaDa', '一名允控', '2020-07-28 10:36:34', '2020-07-28 11:47:09', 'NORMAL');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(60) NOT NULL,
  `role_id` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES (8, 'ce1c735a9889572280feb315ec737bfd', '1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
