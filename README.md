# quick-start(v2)

### 介绍

quick-start(v2)是由YoonaDa整合的一个可快速开始写Crud的Demo,是在v1版本的基础上进行改进。该Demo是基于SpringBoot的2.3.1.RELEASE整合SpringSecurity来实现前后分离权限注解+JWT登录认证,AOP实现自定义日志注解。同时整合了MyBatis-Plus、MySQL5.7、Redis、Swagger2，编写了一些基本的系统异常类、公共枚举、系统响应类等。
支持传统的jar包打包,同时也支持Dockerfile打包。

### 使用步骤
##### 1、 git clone -b v2 https://gitlab.com/YoonaDa/quick-start.git
##### 2、 导入 src/main/resources/sql/ 下的quick-start-v2.sql


